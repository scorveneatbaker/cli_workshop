## cal: Online Calendar

The format of the `cal` command is:

```bash
cal [ month ] year
```
It prints a calendar for the specified year. If a month is specified, a calendar for that month is printed.

Example usage:

```bash
dritchie@cliwkshp:~$ cal --help
dritchie@cliwkshp:~$ cal 1999
```