## cut: remove sections of a file

The format for the `cut` command is:

```bash
cut options directory
```

Example usage:

```bash
dritchie@cliwkshp:~$ cat ~/cli_workshop/data_files/cut_example.txt
tr command for translations.
du command for disk usage
ls command is an interactive calculator
wc command lists number of lines
dritchie@cliwkshp:~$ cat --help
dritchie@cliwkshp:~$ cut -c4 ~/cli_workshop/data_files/cut_example.txt
c
c
c
c
```
extracts only a desired column from a file using the `-c` option.

```bash
dritchie@cliwkshp:~$ cut -c4-6 ~/cli_workshop/data_files/cut_example.txt
com
com
com
com
```
