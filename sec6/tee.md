## tee: add output to file

The `tee` command takes the format of:

```
tee [ -a ] [ File ]
```

The following command only displays output on the screen (stdout)

```bash
dritchie@cliwkshp:~$ pwd
/home/dritchie
```
The same command directs the output to a file

```bash
dritchie@cliwkshp:~$ pwd > file
dritchie@cliwkshp:~$ cat file
/home/dritchie
```
By using a pipe to `tee` we can display on stdout _AND_ direct to a file

```
dritchie@cliwkshp:~$ pwd | tee file.txt
/home/dritchie
```
