# Shell Programming

We described shell as your interface to the Unix Operating System. A shell however is more than an interface, it is also an entire programming language. We will do a whistle-stop tour of the C shell programming language. 

Some of the reasons one would write a shell program are:

1. It is strongly designed for use with Unix commands. It is easy to use existing Unix commands for much of the programming.
2. Shell programs are machine-independent. A shell program running on one type of machine runs on any other type of machine, provided both have the same Unix commands that are used in the shell program. 

It is important to point out that shell programs are generally slower than traditional higher level programs.

## Shell Detour.

Your instructor struggled with this selection of shell programming language to work with. For the purposes of this workshop we will look at the Bourne Shell which is found in all systems. It is the most compact shell and the simplest. 